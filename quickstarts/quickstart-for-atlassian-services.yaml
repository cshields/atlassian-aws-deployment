AWSTemplateFormatVersion: '2010-09-09'
Description: 'Master Template for Atlassian Services'
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
      - Label:
          default: ' VPC Network Configuration'
        Parameters:
          - AccessCIDR
          - AssociatePublicIpAddress
          - AvailabilityZones
          - PrivateSubnet1CIDR
          - PrivateSubnet2CIDR
          - PublicSubnet1CIDR
          - PublicSubnet2CIDR
          - SSLCertificateName
          - VPCCIDR
      - Label:
          default: Amazon EC2 Configuration
        Parameters:
          - KeyPairName
          - NATInstanceType
      - Label:
          default: AWS Quick Start Configuration
        Parameters:
          - DRRegion
          - EnableBackmac
          - EnableForge
          - FlaskSecretKey
          - QSS3BucketName
          - QSS3KeyPrefix
    ParameterLabels:
      AccessCIDR:
        default: Access CIDR
      AvailabilityZones:
        default: Availability Zones
      DRRegion:
        default: DR Region
      EnableBackmac:
        default: Enable Backup Machine
      EnableForge:
        default: Enable Forge
      FlaskSecretKey:
        default: Flask Secret Key
      KeyPairName:
        default: Key Name
      NATInstanceType:
        default: NAT Instance Type
      PrivateSubnet1CIDR:
        default: Private Subnet 1 CIDR
      PrivateSubnet2CIDR:
        default: Private Subnet 2 CIDR
      PublicSubnet1CIDR:
        default: Public Subnet 1 CIDR
      PublicSubnet2CIDR:
        default: Public Subnet 2 CIDR
      QSS3BucketName:
        default: Quick Start S3 Bucket Name
      QSS3KeyPrefix:
        default: Quick Start S3 Key Prefix
      VPCCIDR:
        default: VPC CIDR
Parameters:
  AccessCIDR:
    Default: 0.0.0.0/0
    AllowedPattern: ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$
    Description: CIDR Block allowed to access Atlassian Servcies
    Type: String
  AvailabilityZones:
    Description: 'List of Availability Zones to use for the subnets in the VPC. Note:
      The logical order is preserved and only 2 AZs are used for this deployment.'
    Type: List<AWS::EC2::AvailabilityZone::Name>
  DRRegion:
    AllowedValues:
      - ap-northeast-1
      - ap-northeast-2
      - ap-south-1
      - ap-southeast-1
      - ap-southeast-2
      - ca-central-1
      - eu-central-1
      - eu-north-1
      - eu-west-1
      - eu-west-2
      - eu-west-3
      - sa-east-1
      - us-east-1
      - us-east-2
      - us-west-1
      - us-west-2
    Type: String
    Description: Must be a valid AWS region.
  EnableBackmac:
    Default: true
    AllowedValues:
      - true
      - false
    ConstraintDescription: Must be 'true' or 'false'.
    Description: Should we setup Backup Machine
    Type: String
  EnableForge:
    Default: true
    AllowedValues:
      - true
      - false
    ConstraintDescription: Must be 'true' or 'false'.
    Description: Should we setup Forge
    Type: String
  FlaskSecretKey:
    Description: 'Secret key passed to Flask app to enable sessions, which are required to run. For more info: https://bit.ly/2PRfJRk'
    Type: String
  KeyPairName:
    Description: Public/private key pairs allow you to securely connect to your instance
      after it launches
    Type: AWS::EC2::KeyPair::KeyName
  NATInstanceType:
    Default: t2.small
    AllowedValues:
      - t2.nano
      - t2.micro
      - t2.small
      - t2.medium
      - t2.large
      - m3.medium
      - m3.large
      - m4.large
    Description: Amazon EC2 instance type for the NAT Instances. This is only used
      if the region does not support NAT gateways.
    Type: String
  PrivateSubnet1CIDR:
    Default: 10.0.0.0/19
    AllowedPattern: ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$
    Description: CIDR block for private subnet 1 located in Availability Zone 1.
    Type: String
  PrivateSubnet2CIDR:
    Default: 10.0.32.0/19
    AllowedPattern: ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$
    Description: CIDR block for private subnet 2 located in Availability Zone 2.
    Type: String
  PublicSubnet1CIDR:
    Default: 10.0.128.0/20
    AllowedPattern: ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$
    Description: CIDR Block for the public DMZ subnet 1 located in Availability Zone 1
    Type: String
  PublicSubnet2CIDR:
    Default: 10.0.144.0/20
    AllowedPattern: ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$
    Description: CIDR Block for the public DMZ subnet 2 located in Availability Zone 2
    Type: String
  QSS3BucketName:
    Default: 'atlassian-aws-resources'
    AllowedPattern: ^[0-9a-zA-Z]+([0-9a-zA-Z-]*[0-9a-zA-Z])*$
    ConstraintDescription: Quick Start bucket name can include numbers, lowercase
      letters, uppercase letters, and hyphens (-). It cannot start or end with a hyphen
      (-).
    Description: S3 bucket name for the Quick Start assets. Quick Start bucket name
      can include numbers, lowercase letters, uppercase letters, and hyphens (-).
      It cannot start or end with a hyphen (-).
    Type: String
  QSS3KeyPrefix:
    Default: ''
    AllowedPattern: ^[0-9a-zA-Z-/]*$
    ConstraintDescription: Quick Start key prefix can include numbers, lowercase letters,
      uppercase letters, hyphens (-), and forward slash (/).
    Description: S3 key prefix for the Quick Start assets. Quick Start key prefix
      can include numbers, lowercase letters, uppercase letters, hyphens (-), and
      forward slash (/).
    Type: String
  VPCCIDR:
    Default: 10.0.0.0/16
    AllowedPattern: ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$
    Description: CIDR Block for the VPC
    Type: String
Conditions:
  GovCloudCondition: !Equals
    - !Ref 'AWS::Region'
    - us-gov-west-1
  EnableBackmac:
    !Equals [!Ref EnableBackmac, true]
  EnableForge:
    !Equals [!Ref EnableForge, true]
Mappings:
  AWSInfoRegionMap:
    ap-northeast-1:
      Partition: aws
      QuickStartS3URL: https://s3.amazonaws.com
    ap-northeast-2:
      Partition: aws
      QuickStartS3URL: https://s3.amazonaws.com
    ap-south-1:
      Partition: aws
      QuickStartS3URL: https://s3.amazonaws.com
    ap-southeast-1:
      Partition: aws
      QuickStartS3URL: https://s3.amazonaws.com
    ap-southeast-2:
      Partition: aws
      QuickStartS3URL: https://s3.amazonaws.com
    eu-central-1:
      Partition: aws
      QuickStartS3URL: https://s3.amazonaws.com
    eu-west-1:
      Partition: aws
      QuickStartS3URL: https://s3.amazonaws.com
    sa-east-1:
      Partition: aws
      QuickStartS3URL: https://s3.amazonaws.com
    us-east-1:
      Partition: aws
      QuickStartS3URL: https://s3.amazonaws.com
    us-gov-west-1:
      Partition: aws-us-gov
      QuickStartS3URL: https://s3-us-gov-west-1.amazonaws.com
    us-west-1:
      Partition: aws
      QuickStartS3URL: https://s3.amazonaws.com
    us-west-2:
      Partition: aws
      QuickStartS3URL: https://s3.amazonaws.com
Resources:
  VPCStack:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: !Sub
        - https://${QSS3BucketName}.${QSS3Region}.amazonaws.com/${QSS3KeyPrefix}submodules/quickstart-aws-vpc/templates/aws-vpc.template
        - QSS3Region: !If
            - GovCloudCondition
            - s3-us-gov-west-1
            - s3
      Parameters:
        AvailabilityZones:
          Fn::Join:
            - ','
            - !Ref 'AvailabilityZones'
        KeyPairName: !Ref 'KeyPairName'
        NATInstanceType: !Ref 'NATInstanceType'
        NumberOfAZs: '2'
        PrivateSubnet1ACIDR: !Ref 'PrivateSubnet1CIDR'
        PrivateSubnet2ACIDR: !Ref 'PrivateSubnet2CIDR'
        PublicSubnet1CIDR: !Ref 'PublicSubnet1CIDR'
        PublicSubnet2CIDR: !Ref 'PublicSubnet2CIDR'
        VPCCIDR: !Ref 'VPCCIDR'
  BackmacStack:
    Type: AWS::CloudFormation::Stack
    Condition: EnableBackmac
    DependsOn:
      - VPCStack
    Properties:
      TemplateURL: !Sub
        - https://${QSS3BucketName}.${QSS3Region}.amazonaws.com/${QSS3KeyPrefix}quickstarts/quickstart-backmac-for-atlassian-services.yaml
        - QSS3Region: !If
            - GovCloudCondition
            - s3-us-gov-west-1
            - s3
      Parameters:
        AccessCIDR: !Ref AccessCIDR
        BackmacDRRegion: !Ref DRRegion
        KeyName: !Ref 'KeyPairName'
        InternalSubnets: !Join [ ',', [ !GetAtt VPCStack.Outputs.PrivateSubnet1AID, !GetAtt VPCStack.Outputs.PrivateSubnet2AID ]]
        VPC: !GetAtt VPCStack.Outputs.VPCID
  BastionStack:
    Type: AWS::CloudFormation::Stack
    DependsOn:
      - VPCStack
    Properties:
      TemplateURL: !Sub
        - https://${QSS3BucketName}.${QSS3Region}.amazonaws.com/${QSS3KeyPrefix}quickstarts/quickstart-bastion-for-atlassian-services.yaml
        - QSS3Region: !If
            - GovCloudCondition
            - s3-us-gov-west-1
            - s3
      Parameters:
        AccessCIDR: !Ref AccessCIDR
        KeyName: !Ref 'KeyPairName'
        Subnet: !GetAtt VPCStack.Outputs.PublicSubnet1ID
        VPC: !GetAtt VPCStack.Outputs.VPCID
  ForgeStack:
    Type: AWS::CloudFormation::Stack
    Condition: EnableForge
    DependsOn:
      - VPCStack
    Properties:
      TemplateURL: !Sub
        - https://${QSS3BucketName}.${QSS3Region}.amazonaws.com/${QSS3KeyPrefix}quickstarts/quickstart-forge-for-atlassian-services.yaml
        - QSS3Region: !If
            - GovCloudCondition
            - s3-us-gov-west-1
            - s3
      Parameters:
        AccessCIDR: !Ref AccessCIDR
        FlaskSecretKey: !Ref FlaskSecretKey
        KeyName: !Ref 'KeyPairName'
        ExternalSubnet: !GetAtt VPCStack.Outputs.PublicSubnet1ID
        InternalSubnet: !GetAtt VPCStack.Outputs.PrivateSubnet1AID
        VPC: !GetAtt VPCStack.Outputs.VPCID
Outputs:
  DefaultKey:
    Description: Default Ec2 keypair name
    Value: !Ref 'KeyPairName'
    Export:
      Name: 'ATL-DefaultKey'
  PrivateSubnets:
    Description: A list of 2 Private subnets
    Value: !Join [ ',', [ !GetAtt VPCStack.Outputs.PrivateSubnet1AID, !GetAtt VPCStack.Outputs.PrivateSubnet2AID ]]
    Export:
      Name: 'ATL-PriNets'
  PublicSubnets:
    Description: A list of 2 Public subnets
    Value: !Join [ ',', [ !GetAtt VPCStack.Outputs.PublicSubnet1ID, !GetAtt VPCStack.Outputs.PublicSubnet2ID ]]
    Export:
      Name: 'ATL-PubNets'
  VPCID:
    Description: Atlassian Services VPC ID
    Value: !GetAtt VPCStack.Outputs.VPCID
    Export:
      Name: 'ATL-VPCID'
